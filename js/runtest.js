// JavaScript Document
$(document).ready(function(e) {
    $("#results-div").hide(0);
	
	function ShowResult(resultStr, resultStatus){
		$('#result-placeholder').html(resultStr);
		$("#results-div").show(250);
		if(resultStatus === 1){
			$('#results-header').removeClass('red-text');
			$('#results-header').addClass('green-text');
		}
		else{
			$('#results-header').removeClass('green-text');
			$('#results-header').addClass('red-text');
		}
	}
	
	$('#check-button').click(function(e) {
        e.preventDefault();
		$("#results-div").hide(0);
		var strVal = $('#text_to_check').val();
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				ShowResult('Is a Palindrome', 1);
			}
			else if (this.readyState == 4 && this.status != 200){
				ShowResult('Is not a Palindrome', 0);
			}
		};
		xhttp.open("GET", "https://palindrome-api.herokuapp.com/check/"+strVal, true);
		xhttp.send();
    });
});