
## Intro 
 
To make this app work:
 
1. Clone this git locally.
2. Set up a local webserver (usually localhost) to run the app hosted on this git.
3. Run it in your browser 
4. Fill the input with the desired text and then hit "CHECK!" to check if the inputted text is a palindrome or not.

 
--- 
 
## About the nodejs app 
 
The app was created using Heroku, and it is now available through the URL: https://palindrome-api.herokuapp.com/ .
In case you wanna test it directly from heroku, you need to access: https://palindrome-api.herokuapp.com/check/THE_WORD_YOU_WANNA_CHECK

NOTE: Directly tests work only with words, if you want to try a full text test, you need to do it over the app provided on this repository.
NOTE 2: If the test is successful (therefore, the word is a palindrome) you will se an "OK" when the page loads. Otherwise a "Bad Request" answer (meaning the word is not a palindrome).
 \ No newline at end of file