<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Palindrome Test</title>

  <!-- CSS  -->
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-text">Palindrome Test</h1>
      <div class="row center">
        <h5 class="header col s12 light">Insert the text you want to check if it is a Palindrome.</h5>
      </div>
      <div class="row">
      	<div class="input-field col s12">
          <input id="text_to_check" type="text" class="validate">
          <label for="text_to_check">Type your text here</label>
        </div>
      </div>
      <div class="row center">
        <a href="#" id="check-button" class="btn-large waves-effect waves-light blue">Check!</a>
      </div>
      <div class="row" id="results-div">
      	<h5 class="header col s12 light" id="results-header">RESULTS<hr></h5>
        
        <h5 class="header col s12 " id="result-placeholder">
        	<small>asdadsa</small>
        </h5>
        
      </div>
      <br><br>

    </div>
  </div>

	


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/runtest.js"></script>

  </body>
</html>